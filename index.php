<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Monitoring</title>
        <link href="bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="icon" type="image/png" href="images/favicon.png" />
        <script type="text/javascript" src="justGage.1.0.1/resources/js/justgage.1.0.1.js"></script>
        <script type="text/javascript" src="justGage.1.0.1/resources/js/raphael.2.1.0.min.js"></script>
        <script type="text/javascript" src="justGage.1.0.1/resources/js/justgage.1.0.1.min.js"></script>
    </head>
    <body>
        <header>
            <div class="container">
                <h1>Monitoring<span class="glyphicon glyphicon-home"></span></h1>
                <?php include("uptime.php"); ?>
                <p> Il est <?php echo $time ?></p>
                <hr />
            </div>
        </header>

        <section>
            <div class="container">
                <div class="row">
                   <div class="col-md-4">
                        <h1>Ram</h1>
                        <?php include("ram.php"); ?>
                        <p>Ram total : <?php echo $ram_total; ?>Ko</p>
                        <p>Ram utilisée : <?php echo $ram_use; ?>ko</p>
                        <p>Ram disponible : <?php echo $ram_disponible; ?> Ko</p>
                            <div id="gauge" class="200x160px">
                                <script>
                                    var g = new JustGage({
                                        id: "gauge",        
                                        value: <?php echo $ram_use_mo ?>,
                                        min: 0,
                                        max: <?php echo $ram_total_mo ?>,
                                        title: "Ram", 
                                        label: "Mo",
                                        levelColorsGradient: false
                                   });
                                </script>
                            </div>
                            <div id="bigfella" style="width:400px; height:320px"></div>
                   </div>
                    <div class="col-md-4">
                        <h1>CPU</h1>
                        <?php include("cpu.php"); ?>
                    </div>
                    <div class="col-md-4">
                        <h1>Espace disque</h1>
                        <?php include ("espace_disque.php"); ?>
                    </div>
                </div>
            </div>
        </section>

        <footer>
            <div class = "container">
                <p>Projet 4- 2014-2015</p>
            </div>
        </footer>
        <!-- Dépendance de Bootstrap -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
    </body>
</html>